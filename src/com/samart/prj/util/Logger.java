package com.samart.prj.util;

import android.util.Log;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public final class Logger {

    private static final String TAG = "govnokod";
    private static final boolean IS_DEBUG = false;

    public static void log(final String m) {
        if (IS_DEBUG)
            Log.e(TAG, m);
    }

    public static void logEx(final String m, final Throwable e) {
        if (IS_DEBUG) {
            log(m + ' ' + e + ' ' + e.getMessage());
            //noinspection CallToPrintStackTrace
            e.printStackTrace();
        }
    }
}
