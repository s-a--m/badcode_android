package com.samart.prj.util;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class SearchStringTrie {
    private static final char EC = 'ё' + 1;
    private Node root = new Node();

    public SearchStringTrie(final String[] dict) {
        for (final String word : dict) {
            add(root, word.toLowerCase(), 0, word.length());
        }
    }
    public SearchStringTrie(){}

    public static void main(final String[] args) {
        SearchStringTrie trie = new SearchStringTrie(new String[]{"капуста", "капот", "кот", "кит", "пес", "пот", "кошка", "коты"});
        String s = trie.toString();
        System.out.println(s);
        String[] test = new String[]{"катировка", "котировка", "кот", "ко", "пес", "песок", "п", "петух"};
        for (String testWord : test) {
            System.out.println(testWord + "->" + trie.match(testWord.toCharArray(), 0, testWord.length()));
        }
    }

    private static boolean match(final Node node, final char[] chars, final int pos, final int endPos) {
        if (pos == endPos) return node.isEnd;

        char c = chars[pos];
        if (node.childs[c] != null) {
            return match(node.childs[c], chars, pos + 1, endPos);
        } else {
            if (node.isEnd) return true;
        }
        return false;
    }

    private static void add(Node node, String word, int pos, int endPos) {
        if (pos > endPos) throw new RuntimeException(pos + " " + endPos);
        if (endPos > word.length()) throw new RuntimeException(endPos + " " + word.length());
        char c = word.charAt(pos);
        if (node.childs[c] == null) {
            node.childs[c] = new Node();
        }
        if (pos == endPos - 1) {
            node.childs[c].isEnd = true;
        } else {
            add(node.childs[c], word, pos + 1, endPos);
        }
    }

    private static void printSiblings(StringBuilder sb, Node node, int level) {
        if (null == node) return;
        Node[] ns = node.childs;
        sb.append(" -> ");
        for (int i = 0; i < ns.length; i++) {
            if (ns[i] != null) {
                if (ns[i].isEnd) sb.append(("" + (char) i).toUpperCase());
                else
                    sb.append((char) i);
                printSiblings(sb, ns[i], level + 1);
            }
        }
        sb.append("\r\n");
        for (int j = 0; j < level - 1; j++) sb.append(" ->  ");
    }

    public void addFilter(String charSequence) {
        add(root, charSequence, 0, charSequence.length());
    }

    public boolean match(final char[] chars, final int pos, final int endPos) {
        if (endPos > chars.length) throw new RuntimeException(endPos + " " + chars.length);
        if (pos >= endPos) throw new RuntimeException(pos + " " + endPos);
        if (chars.length == 0) throw new RuntimeException(chars.length + "");
        return match(root, chars, pos, endPos);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        printSiblings(sb, root, 1);
        return sb.toString();
    }

    private static class Node {
        boolean isEnd = false;
        Node[] childs = new Node[EC];
    }
}
