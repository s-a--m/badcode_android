package com.samart.prj.util;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.TextView;

import java.util.Random;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public final class ViewUtils {

    private static final float[] hsv = {360.0f, 0.3f, 0.3f};

    private static final Random colorRnd = new Random();
    private static final int COLOR_MAX_HUE = 360;

    public static void applyRandomBackgroundColor(final TextView v) {
        hsv[0] = colorRnd.nextInt(COLOR_MAX_HUE);
        final int color = Color.HSVToColor(hsv);
        v.setBackgroundColor(color);
    }

    private static Typeface font;

    public static void applyFont(final Context context, final TextView v) {
        synchronized (ViewUtils.class) {
            if (null == font) {
                font = Typeface.createFromAsset(context.getAssets(), "terminusttf4382.otf");
            }
            v.setTypeface(font);
        }
    }

    public static Typeface getFont(final Context context) {
        synchronized (ViewUtils.class) {
            if (null == font) {
                font = Typeface.createFromAsset(context.getAssets(), "terminusttf4382.otf");
            }
            return font;
        }
    }
}
