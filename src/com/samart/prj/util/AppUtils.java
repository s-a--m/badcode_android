package com.samart.prj.util;

import android.app.Activity;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public final class AppUtils {
    public static void closApp() {
        System.exit(0);
    }

    public static void setStyledActionBarTheme(final SherlockActivity activity) {
        // final ActionBar ab = activity.getSupportActionBar();
        //applyThemeToActionBar(activity, ab);

    }

    private static void applyThemeToActionBar(final Activity activity, final ActionBar ab) {
        // This is a workaround for http://b.android.com/15340 from
        // http://stackoverflow.com/a/5852198/132047
        //    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
        //    final BitmapDrawable bg = (BitmapDrawable) activity.getResources()
        //           .getDrawable(R.drawable.ab_background_textured_example);
        //  bg.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        // ab.setBackgroundDrawable(bg);
        //   }

        //  ab.setDisplayHomeAsUpEnabled(true);
    }

    public static void setStyledActionBarTheme(final SherlockFragmentActivity activity) {
        // final ActionBar ab = activity.getSupportActionBar();
        //applyThemeToActionBar(activity, ab);
    }
}
