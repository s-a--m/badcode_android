package com.samart.prj.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

import com.samart.prj.R;
import com.samart.prj.util.ViewUtils;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class LinedTextView extends TextView {

    private static final int COLOR_TEXT = Color.LTGRAY;
    private final Paint mPaint;
    private final Paint mRullerPaint;
    private float mRullerWidth = 20.0f;
    private String[] mText;
    private int mHeight;
    private int mWidth;
    private int mBaseline;
    private int mLinesCount;
    private int mLineHeight;

    public LinedTextView(final Context context) {
        this(context, null, 0);
    }

    public LinedTextView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LinedTextView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        mPaint = new Paint();
        mPaint.setColor(COLOR_TEXT);
        mPaint.setTypeface(ViewUtils.getFont(context));
        mPaint.setAntiAlias(true);
        mRullerPaint = new Paint();
        final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.LinedTextView, 0, 0);
        try {
            final int ruller_color = attributes.getColor(R.styleable.LinedTextView_linesColor, Color.WHITE);
            mRullerPaint.setColor(ruller_color);
        } finally {
            attributes.recycle();
        }
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        final int width = getMeasuredWidth();
        final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        final int height = getMeasuredHeight();
        final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if (width > 0 && height > 0) {
            setMeasuredDimension(MeasureSpec.makeMeasureSpec((int) (width + mRullerWidth), widthMode),
                    MeasureSpec.makeMeasureSpec(height, heightMode));
        }
    }

    @Override
    protected void onLayout(final boolean changed, final int left, final int top, final int right, final int bottom) {
        super.onLayout(false, left, top, right, bottom);
        mWidth = getWidth();
        mHeight = getHeight();
        mLinesCount = Math.max(1, getLineCount());
        mBaseline = getBaseline();
        mLineHeight = mHeight / mLinesCount;
        final int textSize = mLineHeight - 8;
        mPaint.setTextSize(textSize);
        mRullerWidth = 1.5f * textSize;
        mText = new String[mLinesCount];
        for (int i = 0; i < mText.length; i++) {
            mText[i] = String.format("%02d", i + 1);
        }
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        canvas.translate(mRullerWidth, 0.0f);
        super.onDraw(canvas);
        canvas.translate(-mRullerWidth, 0.0f);
        final Paint p = mPaint;
        final Paint linePaint = mRullerPaint;
        final int baseline = mBaseline;
        final int linesCount = mLinesCount;
        final int lineHeight = mLineHeight;
        final int width = mWidth;
        final String[] text = mText;
        canvas.drawRect(0.0f, 0, mRullerWidth, mHeight, linePaint);
        for (int i = 0; i < linesCount; i++) {
            canvas.drawLine(
                    0, baseline + i * lineHeight + 2,
                    width, baseline + i * lineHeight + 2,
                    linePaint);
            canvas.drawText(
                    text[i], 0, text[i].length(),
                    2, baseline + i * lineHeight - 1,
                    p);
        }
    }
}
