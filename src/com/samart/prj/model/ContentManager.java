package com.samart.prj.model;

import android.widget.ListView;
import com.samart.prj.adapter.MainListViewAdapter;
import com.samart.prj.adapter.PaneListViewAdapter;
import com.samart.prj.cache.PostsCache;
import com.samart.prj.util.Logger;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class ContentManager {
    private final PostsCache postsCache;
    private ListView listView;
    public PaneListViewAdapter.Groups g;
    public int dateItem;
    public int typeItem;

    public ContentManager(final PaneListViewAdapter.Groups g, final int dateItem, final int typeItem) {
        postsCache = PostsCache.getInstance();
        this.g = g;
        this.dateItem = dateItem;
        this.typeItem = typeItem;

    }

    public void submitQuery(final PaneListViewAdapter.Groups g, final int dateItem, final int typeItem) {
        this.g = g;
        this.dateItem = dateItem;
        this.typeItem = typeItem;
        postsCache.purge();
        if (null == listView) {
            Logger.log("contentmanager listview == null");
            return;
        }
        final MainListViewAdapter adapter = (MainListViewAdapter) listView.getAdapter();
        adapter.resetItems();
        adapter.notifyDataSetChanged();
    }

    public void setListView(final ListView listView) {
        this.listView = listView;
    }

    public ListView getListView() {
        return listView;
    }

    public int getItemsCount() {
         return postsCache.getItemsCount() ;
    }

    public PostItem getPostItem(final int position) {
        return postsCache.getPostItem(position);
    }
}
