package com.samart.prj.model;

import java.io.Serializable;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class CommentItem implements Serializable{
    public final String user;
    public final String date;
    public final String votes;
    public final String text;
    public final String sid;

    public CommentItem(final String user, final String date, final String votes,
                       final String text, final String sid) {
        this.user = user;
        this.date = date;
        this.votes = votes;
        this.text = SpamFilter.getInstance().filterOutFullText(text);
        this.sid = sid;
    }
}
