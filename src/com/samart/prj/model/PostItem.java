package com.samart.prj.model;

import java.io.Serializable;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class PostItem implements Serializable {
    public final String lang;
    public final String code;
    public final String user;
    public final String desc;
    public final String vote;
    public final String date;
    public final String sid;
    public final String url;
    public final String commentsCount;

    public PostItem(final String lang, final String code, final String user,
                    final String desc, final String vote, final String date,
                    final String sid, final String url, final String commentsCount) {
        this.lang = lang;
        this.code = SpamFilter.getInstance().filterOutFullText(code);
        this.user = user;
        this.desc = SpamFilter.getInstance().filterOutFullText(desc);
        this.vote = vote;
        this.date = date;
        this.sid = sid;
        this.url = url;
        this.commentsCount = commentsCount;
    }

    @Override
    public boolean equals(final Object o) {
        return null != o && (o instanceof PostItem)
                && code != null && ((PostItem) o).code != null
                && code.equals(((PostItem) o).code);
    }

    @Override
    public int hashCode() {
        return null == code ? super.hashCode() : code.hashCode();
    }
}
