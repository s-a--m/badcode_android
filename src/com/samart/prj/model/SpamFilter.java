package com.samart.prj.model;

import android.content.Context;

import com.samart.prj.R;
import com.samart.prj.util.Logger;
import com.samart.prj.util.SearchStringTrie;

import java.util.concurrent.CountDownLatch;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class SpamFilter {

    private static final CountDownLatch WAIT_FOR_INIT = new CountDownLatch(1);
    private final SearchStringTrie stringTrieSearch;
    private boolean isEnabled = true;

    private SpamFilter() {
        stringTrieSearch = new SearchStringTrie();
    }

    public static void initInstance(final Context context) {
        try {
            final String[] array = context.getResources().getStringArray(R.array.badWords);
            SpamFilterHolder.INSTANCE.addFilters(array);
        } finally {
            WAIT_FOR_INIT.countDown();
        }
    }

    public static SpamFilter getInstance() {
        try {
            WAIT_FOR_INIT.await();
        } catch (InterruptedException e) {
            Logger.logEx("failed to await condition lock", e);
        }
        return SpamFilterHolder.INSTANCE;
    }

    private static void fillCensor(final char[] chars, final int startPos, final int endPos) {
        for (int i = startPos; i < endPos; i++) {
            chars[i] = '-';
        }
    }

    /**
     * @param chars    chars array
     * @param startPos start position
     * @return position of first space char after start pos
     */
    private static int findNextSpace(final char[] chars, final int startPos) {
        int pos = startPos + 1;
        while (pos < chars.length) {
            if (chars[pos] != 'ё' && (chars[pos] == ' ' || chars[pos] < 'а' || chars[pos] > 'я'))
                return pos;
            pos++;
        }
        return pos;
    }

    /**
     * @param chars    chars array
     * @param startPos start search position
     * @return position of first non space character
     */
    private static int skipSpaces(final char[] chars, final int vStartPos) {
        int startPos = vStartPos;
        while (startPos < chars.length) {
            if (chars[startPos] >= 'а' && chars[startPos] <= 'я' || chars[startPos] == 'ё')
                return startPos;
            startPos++;
        }
        return startPos;
    }

    private void addFilters(final String[] array) {
        for (final String s : array)
            stringTrieSearch.addFilter(s);
    }

    public void setEnabled(final boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public String filterOutFullText(final String inString) {
        if (!isEnabled) return inString;
        int startPos = 0;
        final char[] chars = inString.toLowerCase().toCharArray();
        final char[] charsNormalCase = inString.toCharArray();
        startPos = skipSpaces(chars, startPos);
        int endPos = findNextSpace(chars, startPos);
        while (startPos < chars.length && endPos <= chars.length) {
            if (stringTrieSearch.match(chars, startPos, endPos))
                fillCensor(charsNormalCase, startPos, endPos);
            startPos = skipSpaces(chars, endPos);
            endPos = findNextSpace(chars, startPos);

        }
        return new String(charsNormalCase);
    }

    private static final class SpamFilterHolder {
        private static final SpamFilter INSTANCE = new SpamFilter();
    }
}
