package com.samart.prj.model;

import android.text.Html;
import com.samart.prj.adapter.PaneListViewAdapter;
import com.samart.prj.util.Logger;
import com.samart.prj.util.StreamUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class ItemsLoaderRunnable implements Runnable {

    public static final int MAX_RECONNECT_ATTEMPTS = 2;
    private final int pageToLoad;
    private final OnTaskEndedListener listener;
    private final int typeItem;
    private final int dateItem;
    private final PaneListViewAdapter.Groups g;

    public ItemsLoaderRunnable(final PaneListViewAdapter.Groups g, final int dateItem, final int typeItem,
                               final int nextPageToLoad, final OnTaskEndedListener listener) {

        pageToLoad = nextPageToLoad;
        this.listener = listener;
        this.g = g;
        this.dateItem = dateItem;
        this.typeItem = typeItem;
    }

    public interface OnTaskEndedListener {
        void onSuccess(final int loadedPage, final List<PostItem> postItems);

        void onFail(final int loadedPage);
    }


    @Override
    public void run() {
        final WorkerContext parserContext = new WorkerContext(pageToLoad, g, dateItem, typeItem);
        final List<PostItem> postItems = parserContext.doWork();
        if (null == postItems || postItems.isEmpty()) {
            listener.onFail(pageToLoad);
        } else {
            listener.onSuccess(pageToLoad, postItems);
        }

    }

    enum ParserActions implements WorkerAction {
        FIND_CONTENT {
            Pattern pContent = Pattern.compile(".*?<div\\s+?id=\"content\">.*?");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pContent.matcher(context.line);
                if (matcher.find()) {
                    context.parserAction = FIND_POST;
                }
            }
        },
        FIND_POST {
            Pattern pChapter =
                    Pattern.compile(
                            ".*?<a\\s+?rel=\"bookmark\"\\s+?class=\"entry-title\"\\s+?href=\"(.*?)\">Говнокод\\s+?#(\\d+?)</a>\\s*?</h2>.*"
                    );
            Pattern pPaginator =
                    Pattern.compile(
                            ".*?<ul\\s+?class=\"pagination\"><li>.*?</li><li><a\\s+?href=\"http://govnokod\\.ru/.*?\\?page=(\\d+)\">.*");

            @Override
            public void run(final WorkerContext context) {
                if (WorkerContext.lastPage < 0) {
                    final Matcher matcher = pPaginator.matcher(context.line);
                    //Logger.log("search page number " + context.line);
                    if (matcher.matches()) {
                        try {
                            final String spage = matcher.group(1);
                            //       Logger.log("page number finded " + spage);
                            WorkerContext.lastPage = Integer.parseInt(spage);
                        } catch (final NumberFormatException ignore) {
                        }
                    }
                }
                final Matcher matcher = pChapter.matcher(context.line);
                if (matcher.matches()) {
                    context.postUrl = matcher.group(1);
                    context.postSid = matcher.group(2);
                    context.parserAction = FIND_VOTE;
                }
            }
        },
        FIND_VOTE {
            Pattern pVote =
                    Pattern.compile(
                            ".*?<strong\\s+?class=\".*?\"\\s+?title=\"(.*?)\">.*?</strong>.*?");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pVote.matcher(context.line);
                if (matcher.matches()) {
                    context.voteString = matcher.group(1);
                    context.parserAction = FIND_CODE;
                }
            }
        },
        FIND_CODE {
            Pattern pCode = Pattern.compile(".*?<pre><code\\s+?class=\"(.*?)\">(.*)");
            Pattern pEnd = Pattern.compile(".*?<pre><code\\s+?class=\"(.*?)\">(.*?)</code></pre>.*");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pCode.matcher(context.line);
                if (matcher.matches()) {
                    context.langString = matcher.group(1);
                    context.codeStringBuilder = new StringBuilder(300).append("<pre>");
                    final Matcher matcher1 = pEnd.matcher(context.line);
                    if (matcher1.matches()) {
                        context.codeStringBuilder.append(matcher1.group(2)).append("<br>");
                        context.parserAction = FIND_DESCRIPTION;
                    } else {
                        context.codeStringBuilder.append(matcher.group(2)).append("<br>");
                        context.parserAction = PARSE_CODE;
                    }
                }
            }
        },
        PARSE_CODE {
            Pattern pEnd = Pattern.compile("(.*?)</code></pre>.*");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pEnd.matcher(context.line);
                if (matcher.matches()) {
                    context.codeStringBuilder.append(matcher.group(1)).append("</pre>");
                    context.parserAction = FIND_DESCRIPTION;
                } else {
                    context.codeStringBuilder.append(context.line).append("<br>");
                }
            }
        },
        FIND_DESCRIPTION {
            Pattern pDesc = Pattern.compile(".*?<p\\s+?class=\"description\">.*?");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pDesc.matcher(context.line);
                if (matcher.find()) {
                    context.descriptionBuilder = new StringBuilder(100);
                    context.parserAction = PARSE_DESCRIPTION;
                }
            }
        },
        PARSE_DESCRIPTION {
            Pattern pEndDesc = Pattern.compile(".*?</p>.*?");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pEndDesc.matcher(context.line);
                if (matcher.find()) {
                    context.parserAction = PARSE_USER;
                } else {
                    context.descriptionBuilder.append(context.line).append('\n');
                }
            }
        },
        PARSE_USER {
            Pattern pUser = Pattern.compile(
                    ".*?<a\\s+?href=\"http://govnokod\\.ru/user/\\d+\"><img src=\".*?\"\\s+?alt=\".*?\"\\s+?class=\"avatar\"\\s+?/></a>\\s+?<a\\s+?href=\"http://govnokod\\.ru/user/\\d+?\">(.*?)</a>.*?"
            );

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pUser.matcher(context.line);
                if (matcher.matches()) {
                    context.userString = matcher.group(1);
                    context.parserAction = FIND_DATE;
                }
            }
        },
        FIND_DATE {

            Pattern pDate =
                    Pattern.compile(".*?<abbr\\s+?title=\"(.*?)\">.*?</abbr>.*?");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pDate.matcher(context.line);
                if (matcher.matches()) {
                    context.dateString = matcher.group(1);
                    context.parserAction = FIND_COMMENTS_COUNT;
                }
            }
        },
        FIND_COMMENTS_COUNT{
            Pattern pComments = Pattern.compile(".*?<span\\s+?class=\"entry-comments-count\">\\((\\d+?)\\)</span>");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pComments.matcher(context.line);
                if(matcher.matches()){
                    String source1 = context.codeStringBuilder.toString().replace(" ","&nbsp;");
                    final PostItem postItem =
                            new PostItem(context.langString,
                                    Html.fromHtml(source1).toString(),
                                    context.userString,
                                    Html.fromHtml(context.descriptionBuilder.toString()).toString(),
                                    context.voteString,
                                    context.dateString,
                                    context.postSid,
                                    context.postUrl,
                                    matcher.group(1)
                            );
                    context.items.add(postItem);
                    context.parserAction = FIND_POST;
                }
            }
        }

    }

    enum WorkerActions implements WorkerAction {
        CONNECT {
            @Override
            public void run(final WorkerContext context) {
                try {
                    context.urlConnection = (HttpURLConnection) context.url.openConnection();
                    context.urlConnection.connect();
                    final int response = context.urlConnection.getResponseCode();
                    context.action = 200 == response ? READ_ANSWER : RECONNECT;
                } catch (final IOException e) {
                    Logger.logEx("Could not connect " + context.url, e);
                    context.action = RECONNECT;
                }
            }
        },
        READ_ANSWER {
            @Override
            public void run(final WorkerContext context) {
                try {
                    context.reader = new BufferedReader(
                            new InputStreamReader(context.urlConnection.getInputStream()));
                    context.readAnswer();
                    context.action = END;
                } catch (final IOException e) {
                    Logger.logEx("Could not read server answer " + context.url, e);
                    context.action = RECONNECT;
                } finally {
                    StreamUtils.silentlyClose(context.urlConnection, context.reader);
                }
            }

        },
        RECONNECT {
            @Override
            public void run(final WorkerContext context) {
                StreamUtils.silentlyClose(context.urlConnection, context.reader);
                context.action =
                        context.attempt++ > MAX_RECONNECT_ATTEMPTS
                                ? END : CONNECT;
            }
        },
        END {
            @Override
            public void run(final WorkerContext context) {
            }
        }
    }

    interface WorkerAction {
        void run(WorkerContext context);
    }

    public static class WorkerContext {
        public URL url = null;
        public HttpURLConnection urlConnection;
        public WorkerActions action = WorkerActions.CONNECT;
        public int attempt;
        public BufferedReader reader;
        public List<PostItem> items = new ArrayList<PostItem>(10);
        public String line;
        public ParserActions parserAction = ParserActions.FIND_CONTENT;
        public String postUrl;
        public String postSid;
        public String voteString;
        public String langString;
        public StringBuilder codeStringBuilder;
        public StringBuilder descriptionBuilder;
        public String userString;
        public static int lastPage = -1;
        private static final String[] LANG_URLS = {
                "php",
                "cpp",
                "csharp",
                "javascript",
                "java",
                "kucha",
                "c",
                "pascal",
                "sql",
                "actionscript3",
                "python",
                "1c",
                "objc",
                "perl",
                "bash",
                "ruby",
                "vb",
                "asm",};
        private static final String[] bestType = {"", "comments"};
        private static final String[] timeType = {"day", "week", "month", "ever"};
        public String dateString;

        public WorkerContext(final int page, final PaneListViewAdapter.Groups g,
                             final int dateItem, final int typeItem) {

            final StringBuilder sb = new StringBuilder(100);

            sb.append("http://govnokod.ru/");
            switch (g) {
                case ALL:
                    break;
                case LANG:
                    sb.append(LANG_URLS[dateItem]);
                    break;
                case BEST:
                    sb.append("best/").append(bestType[typeItem]).append("?time=").append(timeType[dateItem]);
                    break;
                default:
                    throw new RuntimeException("new items group?");
            }

            if (lastPage > 0) {
                sb.append("?page=").append(lastPage - page);
            }

            try {
                url = new URL(sb.toString());
            } catch (final MalformedURLException ignored) {
            }

            Logger.log("load page url " + lastPage + ' ' + url);
        }

        public List<PostItem> doWork() {
            while (action != WorkerActions.END) {
                //     Logger.log("loader action " + action.name());
                action.run(this);
            }
            return items;
        }

        /**
         * calls after reader created
         */
        public void readAnswer() {
            if (null == reader) return;
            try {
                while ((line = reader.readLine()) != null) {
                    //        Logger.log("parser action " + parserAction.name());
                    //       Logger.log(line);
                    parserAction.run(this);
                }
            } catch (final IOException e) {
                Logger.logEx("Could not read stream ", e);
            }

        }
    }
}
