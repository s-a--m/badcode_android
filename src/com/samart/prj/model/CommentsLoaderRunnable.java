package com.samart.prj.model;

import android.text.Html;
import com.samart.prj.util.Logger;
import com.samart.prj.util.StreamUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class CommentsLoaderRunnable implements Runnable {
    private final PostItem postItem;
    private final OnTaskEnded listener;
    private final List<CommentItem> items = new ArrayList<CommentItem>(100);

    @Override
    public void run() {
        final boolean b = loadItems();
        if (null == listener) return;
        if (b) {
            listener.onSuccess(items);
        } else {
            listener.onFail();
        }
    }

    private boolean loadItems() {
        return new WorkerContext(items, postItem).doWork();
    }

    public List<CommentItem> getItems() {
        return items;
    }

    public interface OnTaskEnded {
        void onSuccess(final List<CommentItem> items);

        void onFail();
    }

    public CommentsLoaderRunnable(final PostItem postItem, final OnTaskEnded listener) {
        this.postItem = postItem;
        this.listener = listener;
    }

    interface ParserWorker {
        void run(WorkerContext context);
    }

    enum ConnectionStates implements ParserWorker {
        CONNECT {
            @Override
            public void run(final WorkerContext context) {
                try {
                    context.connection = (HttpURLConnection) context.url.openConnection();
                    context.connection.addRequestProperty("X-Requested-With", "XMLHttpRequest");
                    context.connection.connect();
                    context.connectionState = READ_ANSWER;
                } catch (final IOException e) {
                    Logger.logEx("io exception ", e);
                    context.connectionState = RECONNECT;
                }
            }
        },
        READ_ANSWER {
            @Override
            public void run(final WorkerContext context) {
                try {
//                    if (200 == context.connection.getResponseCode()) {
                    context.reader = new BufferedReader(new InputStreamReader(context.connection.getInputStream()));
                    context.connectionState = PARSE_HTML;
                    //                  } else {
                    //                     context.connectionState = RECONNECT;
                    //                }
                } catch (final IOException e) {
                    Logger.logEx("io exception read answer ", e);
                    context.connectionState = RECONNECT;
                }
            }
        }, RECONNECT {
            @Override
            public void run(final WorkerContext context) {
                if (context.reconnectAttempts++ > WorkerContext.MAX_RECONNECT_ATTEMPTS) {
                    context.connectionState = END;
                } else {
                    StreamUtils.silentlyClose(context.connection, context.reader);
                    context.connectionState = CONNECT;
                }
            }
        }, PARSE_HTML {
            @Override
            public void run(final WorkerContext context) {
                try {
                    while ((context.line = context.reader.readLine()) != null) {
                        //        Logger.log(context.parserState.name() + ' ' + context.line);
                        context.parserState.run(context);
                    }
                } catch (final IOException e) {
                    Logger.logEx("error while reading server stream ", e);
                }
                context.connectionState = END;
            }
        }, END {
            @Override
            public void run(final WorkerContext context) {
                StreamUtils.silentlyClose(context.connection, context.reader);
            }
        }
    }

    enum ParserStates implements ParserWorker {
        FIND_COMMENT_BEGIN {
            Pattern pCommentId = Pattern.compile(".*?<div id=\"comment-(\\d+?)\" class=\"entry-comment-wrapper.*?\">.*?");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pCommentId.matcher(context.line);
                if (matcher.matches()) {
                    context.csid = matcher.group(1);
                    context.parserState = FIND_AUTHOR;
                }
            }
        }, FIND_AUTHOR {
            Pattern pAuthor =
                    Pattern.compile(
                            ".*?<strong class=\"entry-author\"><a href=\"http://govnokod\\.ru/user/\\d+?\">(.*?)</a></strong>.*?");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pAuthor.matcher(context.line);

                if (matcher.matches()) {
                    context.cuser = matcher.group(1);
                    context.parserState = FIND_DATE;
                }
            }
        }, FIND_DATE {
            Pattern pDate =
                    Pattern.compile(
                            ".*?<abbr class=\"published\" title=\".*?\">(.*?)</abbr>.*?");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pDate.matcher(context.line);

                if (matcher.matches()) {
                    context.cdate = matcher.group(1);
                    context.parserState = FIND_VOTE;
                }
            }
        }, FIND_VOTE {
            Pattern pVote =
                    Pattern.compile(
                            ".*?<strong.*?title=\"(.*?)\">.*?</strong>.*?");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pVote.matcher(context.line);

                if (matcher.matches()) {
                    context.cvote = matcher.group(1);
                    context.parserState = FIND_TEXT_BEGIN;
                }
            }
        }, FIND_TEXT_BEGIN {
            Pattern pBegin = Pattern.compile(".*?<div class=\"entry-comment\"><span class=\"comment-text\">(.*)");
            Pattern pEnd = Pattern.compile("(.*?)</span></div>.*?");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pBegin.matcher(context.line);

                if (matcher.matches()) {
                    final String s = matcher.group(1);
                    final Matcher matcher1 = pEnd.matcher(s);
                    if (matcher1.matches()) {
                        context.ctext = new StringBuilder(matcher1.group(1));
                        createComment(context);
                        context.parserState = FIND_COMMENT_BEGIN;
                    } else {
                        context.ctext = new StringBuilder(s).append("<br/>");
                        context.parserState = FIND_TEXT_END;
                    }
                }
            }
        }, FIND_TEXT_END {
            public Pattern pEnd = Pattern.compile("(.*?)</span></div>.*?");

            @Override
            public void run(final WorkerContext context) {
                final Matcher matcher = pEnd.matcher(context.line);

                if (matcher.matches()) {
                    context.ctext.append(matcher.group(1));
                    createComment(context);
                    context.parserState = FIND_COMMENT_BEGIN;
                } else {
                    context.ctext.append(context.line).append("<br/>");
                }
            }
        };

        private static void createComment(final WorkerContext context) {
            final CommentItem item = new CommentItem(
                    context.cuser,
                    context.cdate,
                    context.cvote,
                    Html.fromHtml(context.ctext.toString()).toString(),
                    context.csid
            );
            context.items.add(item);
            context.success = true;
        }

    }

    private static class WorkerContext {
        ParserStates parserState = ParserStates.FIND_COMMENT_BEGIN;
        static final int MAX_RECONNECT_ATTEMPTS = 2;
        int reconnectAttempts;
        final List<CommentItem> items;
        URL url;
        String line;
        HttpURLConnection connection;
        BufferedReader reader;
        boolean success;
        ConnectionStates connectionState = ConnectionStates.CONNECT;
        public StringBuilder ctext;
        public String cvote;
        public String cdate;
        public String cuser;
        public String csid;

        private WorkerContext(final List<CommentItem> items, final PostItem postItem) {
            this.items = items;
            try {
                url = new URL("http://govnokod.ru/" + postItem.sid + "?onlyComments=true");
            } catch (final MalformedURLException ignored) {
            }
        }

        public boolean doWork() {
            while (connectionState != ConnectionStates.END) {
//                Logger.log(connectionState.name());
                connectionState.run(this);
            }
            return success;
        }
    }
}
