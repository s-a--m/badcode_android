package com.samart.prj.cache;

import com.samart.prj.model.PostItem;

import java.util.ArrayList;
import java.util.List;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class PostsCache {
    public void purge() {
     synchronized (items){
         items.clear();
     }
    }

    private interface Holder {
        PostsCache INSTANCE = new PostsCache();
    }

    public static PostsCache getInstance() {
        return Holder.INSTANCE;
    }

    private final List<PostItem> items = new ArrayList<PostItem>(100);

    public void addItems(final Iterable<PostItem> newItems) {
        for (final PostItem item : newItems) {
            synchronized (items) {
                if (!items.contains(item)) {
                    items.add(item);
                }
            }
        }
    }

    public int getItemsCount() {
        synchronized (items) {
            return items.size();
        }
    }

    public PostItem getPostItem(final int pos) {
        synchronized (items) {
            return items.get(pos);
        }
    }

}
