package com.samart.prj.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.samart.prj.R;
import com.samart.prj.activity.GovnokodActivityMain;
import com.samart.prj.adapter.MainListViewAdapter;
import com.samart.prj.controller.UiController;
import com.samart.prj.model.ContentManager;
import com.samart.prj.util.Logger;
import com.samart.prj.views.AnimatingProgressView;

import java.lang.ref.WeakReference;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class MainFragment extends Fragment {
    private ContentManager contentManager;
    private UiController uiController;
    private WeakReference<ListView> listViewRef;
    private WeakReference<AnimatingProgressView> progressImage;

    public MainFragment() {
    }

    public MainFragment(final ContentManager contentManager, final UiController uiController) {
        this.contentManager = contentManager;
        this.uiController = uiController;
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Logger.log("fragment onActivityCreated");
        final Activity activity = getActivity();
        if (!(activity instanceof GovnokodActivityMain))
            throw new IllegalStateException("this fragment is only for main activty");
        final GovnokodActivityMain gActivity = (GovnokodActivityMain) activity;
        contentManager = gActivity.getContentManager();
        uiController = gActivity.getUiController();

        applyListAdapter(activity);
    }

    private void applyListAdapter(final Activity activity) {
        final ListView listView = listViewRef.get();
        if (null == listView) {
            Logger.log("lost listView");
        } else {
            Logger.log("listview set adapter");

            final MainListViewAdapter adapter = new MainListViewAdapter(activity, contentManager, uiController);
            listView.setAdapter(adapter);
            listView.setOnScrollListener(adapter);

            contentManager.setListView(listView);
        }
        final AnimatingProgressView view = progressImage.get();
        if (null != view) {
            uiController.setProgressView(view);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.main_fragment_layout, null);
        if (view == null) {
            throw new IllegalArgumentException("could not inflate main_fragment_layout");
        }
        listViewRef = new WeakReference<ListView>((ListView) view.findViewById(R.id.main_list));
        progressImage =
                new WeakReference<AnimatingProgressView>(
                        (AnimatingProgressView) view.findViewById(R.id.imageProgress));
        Logger.log("fragment onCreateView");
        return view;
    }
}
