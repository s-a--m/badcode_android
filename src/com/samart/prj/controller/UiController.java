package com.samart.prj.controller;

import com.samart.prj.views.AnimatingProgressView;
import com.samart.slidingmenu.SideMenuLayout;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class UiController {
    private final SideMenuLayout sideMenu;
    private AnimatingProgressView progressView;

    public UiController(final SideMenuLayout sideMenu) {
        this.sideMenu = sideMenu;
    }

    public void setStartLoading() {
        //sideMenu.close();
        if (null != progressView) {
            progressView.start();
        }
    }

    public void setStopLoading() {
        if (null != progressView) {
            progressView.stop();
        }
    }

    public void setProgressView(final AnimatingProgressView progressView) {
        this.progressView = progressView;
    }

    public AnimatingProgressView getProgressView() {
        return progressView;
    }
}
