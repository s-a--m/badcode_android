package com.samart.prj.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class AnimatingProgressView extends ImageView implements Runnable {
    private static final long UFPS = 10;
    public static final int MAX_HSV = 360;
    public static final double DEG_TO_RAD = Math.PI / 360;

    public AnimatingProgressView(final Context context) {
        super(context);
    }

    public AnimatingProgressView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public AnimatingProgressView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    private final float[] hsv = {1.0f, 1.0f, 1.0f};

    private int time;
    private boolean isStopped = true;

    public void start() {
        isStopped = false;
        post(this);
    }

    public void stop() {
        isStopped = true;
        post(this);
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        if (isStopped) {
            return;
        }
        time = ++time % MAX_HSV;
        hsv[0] = time;
        final float s = (float) Math.sin(time * DEG_TO_RAD);
        hsv[1] = s;
        hsv[2] = s;
        final int lcolor = Color.HSVToColor(hsv);
        canvas.drawColor(lcolor);
        postInvalidateDelayed(UFPS);
    }

    @Override
    public void run() {
        if (isStopped) {
            setVisibility(GONE);
        } else {
            setVisibility(VISIBLE);
            invalidate();
        }
    }
}
