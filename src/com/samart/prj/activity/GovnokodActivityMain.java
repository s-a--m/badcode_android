package com.samart.prj.activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.widget.ExpandableListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.samart.prj.R;
import com.samart.prj.adapter.PaneListViewAdapter;
import com.samart.prj.controller.UiController;
import com.samart.prj.fragment.MainFragment;
import com.samart.prj.model.ContentManager;
import com.samart.prj.model.SpamFilter;
import com.samart.prj.util.AppUtils;
import com.samart.prj.util.Logger;
import com.samart.slidingmenu.SideMenuLayout;

public class GovnokodActivityMain extends SherlockFragmentActivity {

    private SideMenuLayout sideMenu;
    private ContentManager contentManager;
    private UiController uiController;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final ActionBar ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);
        AppUtils.setStyledActionBarTheme(this);

        SpamFilter.initInstance(this.getApplicationContext());
        sideMenu = (SideMenuLayout) findViewById(R.id.side_menu);
        uiController = new UiController(sideMenu);
        final ExpandableListView pane = (ExpandableListView) sideMenu.findViewById(R.id.pane_list);
        contentManager = new ContentManager(PaneListViewAdapter.Groups.ALL, 0, 0);
        pane.setAdapter(new PaneListViewAdapter(this, contentManager));
        manageFragments();
        Logger.log("activity onCreate");
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                toggleMenu();
                break;
            case R.id.menu_close:
                finish();
                AppUtils.closApp();
                break;
            case R.id.menu_settings:
                showSettings();
                break;
        }
        return false;
    }


    private void showSettings() {

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        Logger.log("activity onCreateOptionsMenu");
        getSupportMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.log("activity onResume");
    }

    public void openMenu() {
        if (null != sideMenu)
            sideMenu.open();
    }

    public void closeMenu() {
        if (null != sideMenu)
            sideMenu.close();
    }

    public void toggleMenu() {
        if (null != sideMenu) {
            sideMenu.toggleOpenClose();
        }
    }

    private void manageFragments() {
        final FragmentManager fragmentMan = getSupportFragmentManager();
        android.support.v4.app.Fragment fragment = fragmentMan.findFragmentByTag(MainFragment.class.getName());
        if (null == fragment) {
            fragment = new MainFragment(contentManager, uiController);
        }
        final android.support.v4.app.FragmentTransaction transaction = fragmentMan.beginTransaction();

        transaction.replace(R.id.fragment_holder, fragment);
        transaction.commit();
    }

    public ContentManager getContentManager() {
        return contentManager;
    }

    public UiController getUiController() {
        return uiController;
    }
}
