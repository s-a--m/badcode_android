package com.samart.prj.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.samart.prj.R;
import com.samart.prj.adapter.DetailsContentAdapter;
import com.samart.prj.model.PostItem;
import com.samart.prj.util.AppUtils;
import com.samart.prj.util.Logger;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class DetailActivity extends SherlockActivity {
    public static final String TAG_ITEM = "item";
    private PostItem item;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();
        if (null != intent) {
            final Object obj = intent.getSerializableExtra(TAG_ITEM);
            if (obj instanceof PostItem) {
                item = (PostItem) obj;
            }
        }
        if (null == item) {
            Logger.log("no intent bundle included");
            finish();
        }

        final ActionBar ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);
        AppUtils.setStyledActionBarTheme(this);
        final LayoutInflater inflater = getLayoutInflater();
        final View v = inflater.inflate(R.layout.details_layout, null);
        setContentView(v);
        final ListView listView = (ListView) v.findViewById(R.id.listContent);
        listView.setAdapter(new DetailsContentAdapter(this, item));
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return false;
    }

}
