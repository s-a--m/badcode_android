package com.samart.prj.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.samart.prj.R;
import com.samart.prj.model.CommentItem;
import com.samart.prj.model.CommentsLoaderRunnable;
import com.samart.prj.model.PostItem;
import com.samart.prj.util.ViewUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class DetailsContentAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    private final View mainView;
    private final List<CommentItem> comments = new ArrayList<CommentItem>(100);

    public DetailsContentAdapter(final Activity activity, final PostItem item) {
        inflater = activity.getLayoutInflater();
        loadCommentsAsync(item);
        mainView = inflateMainView(inflater, item, activity.getApplicationContext());
    }

    private static class CommentHolder {
        TextView user;
        TextView date;
        TextView vote;
        TextView text;
        int pos = -1;
    }


    private static void fillComment(final CommentItem item, final CommentHolder holder) {
        holder.user.setText(item.user);
        holder.date.setText(item.date);
        holder.vote.setText(item.votes);
        holder.text.setText(item.text);
    }

    private static CommentHolder createCommentHolder(final View v) {
        final CommentHolder holder = new CommentHolder();
        holder.user = (TextView) v.findViewById(R.id.comment_author);
        holder.date = (TextView) v.findViewById(R.id.comment_date);
        holder.vote = (TextView) v.findViewById(R.id.comment_vote);
        holder.text = (TextView) v.findViewById(R.id.comment_text_view);
        return holder;
    }

    private View getCommentView(final int commentIndex, final View convertView, ViewGroup parent) {
        if (commentIndex < 0 || commentIndex > comments.size()) {
            return null;
        }
        View v = convertView;
        CommentHolder holder;
        Object oholder = v == null ? null : v.getTag();
        if (null == v || null == oholder || !(oholder instanceof CommentHolder)) {
            v = inflater.inflate(R.layout.comment_view_item, parent, false);
            holder = createCommentHolder(v);
            v.setTag(holder);
            oholder = holder;
        }
        holder = (CommentHolder) oholder;
        if (commentIndex != holder.pos) {
            holder.pos = commentIndex;
            final CommentItem item = comments.get(commentIndex);
            fillComment(item, holder);
        }
        return v;
    }


    private static View inflateMainView(final LayoutInflater inflater, final PostItem item, final Context context) {
        final View v = inflater.inflate(R.layout.code_view_item, null);
        final TextView textCode = (TextView) v.findViewById(R.id.code_text_view);
        textCode.setText(item.code);
        ViewUtils.applyFont(context, textCode);
        final TextView textUsername = (TextView) v.findViewById(R.id.code_author);
        textUsername.setText(item.user);
        final TextView textDescription = (TextView) v.findViewById(R.id.code_descr_text_view);
        textDescription.setText(item.desc);
        final Button button = (Button) v.findViewById(R.id.commentsCount);
        button.setVisibility(View.GONE);
        final TextView textVote = (TextView) v.findViewById(R.id.code_vote);
        textVote.setText(item.vote);
        final TextView textDate = (TextView) v.findViewById(R.id.code_date);
        textDate.setText(item.date);
        return v;
    }

    private void loadCommentsAsync(final PostItem item) {
        final CommentsLoaderRunnable commentsLoader = new CommentsLoaderRunnable(item, null);
        new AsyncTask<Void, Void, List<CommentItem>>() {
            @Override
            protected List<CommentItem> doInBackground(final Void... params) {
                commentsLoader.run();
                return commentsLoader.getItems();
            }

            @Override
            protected void onPostExecute(final List<CommentItem> commentItems) {
                if (null != commentItems && !commentItems.isEmpty()) {
                    comments.addAll(commentItems);
                    notifyDataSetChanged();
                }
            }
        }.execute();
    }

    interface ItemType {
        int getItemViewType();
    }

    interface ViewInflater {
        View getView(int position, View convertView, DetailsContentAdapter adapter, ViewGroup parent);
    }

    private enum DetailsType implements ItemType, ViewInflater {
        MAIN {
            @Override
            public View getView(final int position, final View convertView, final DetailsContentAdapter adapter, ViewGroup parent) {
                return adapter.mainView;
            }
        }, COMMENT {
            @Override
            public View getView(final int position, final View convertView, final DetailsContentAdapter adapter, ViewGroup parent) {
                return adapter.getCommentView(position - 1, convertView, parent);
            }
        };
        public static final DetailsType[] types = DetailsType.values();

        public static DetailsType fromPosition(final int position) {
            return 0 == position ? MAIN : COMMENT;
        }

        @Override
        public int getItemViewType() {
            return ordinal();
        }
    }


    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        return DetailsType.fromPosition(position).getView(position, convertView, this, parent);
    }

    @Override
    public int getItemViewType(final int position) {
        return DetailsType.fromPosition(position).getItemViewType();
    }


    @Override
    public int getCount() {
        return comments.size() + 1;
    }

    @Override
    public Object getItem(final int position) {
        return null;
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

}
