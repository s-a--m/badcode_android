package com.samart.prj.adapter;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.samart.prj.R;
import com.samart.prj.activity.DetailActivity;
import com.samart.prj.controller.UiController;
import com.samart.prj.model.ContentManager;
import com.samart.prj.model.PostItem;
import com.samart.prj.util.Logger;
import com.samart.prj.util.ViewUtils;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class MainListViewAdapter extends BaseAdapter implements AbsListView.OnScrollListener, View.OnClickListener {
    private static final int MESSAGE_NOTIFY_DATA_SET_CHANGED = 0;
    private final LayoutInflater inflater;
    private final PostsLoader postsLoader;
    private final Handler handler;
    private final ContentManager contentManager;
    private final Context context;

    public MainListViewAdapter(final Activity activity, final ContentManager contentManager, final UiController uiController) {
        inflater = activity.getLayoutInflater();
        context = activity.getApplicationContext();
        postsLoader = new PostsLoader(this, contentManager, uiController);
        this.contentManager = contentManager;
        handler = new NotifyHandler(this);
    }

    /**
     * calls from background thread
     * Notifies adapter that content changed
     */
    public void notifyWithHandler() {
        handler.sendEmptyMessage(MESSAGE_NOTIFY_DATA_SET_CHANGED);
    }

    @Override
    public int getCount() {
        return contentManager.getItemsCount();
    }

    @Override
    public Object getItem(final int position) {
        return position;
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View v = convertView;
        final CodeViewHolder holder;
        if (null == v) {
            holder = new CodeViewHolder();
            v = inflateCodeView(holder, parent);
            v.setTag(holder);
        } else {
            holder = (CodeViewHolder) v.getTag();
        }
        if (holder.pos != position) {
            holder.pos = position;
            fillCodeView(holder, position);
        }
        return v;

    }


    @Override
    public void onScrollStateChanged(final AbsListView view, final int scrollState) {
    }

    @Override
    public void onScroll(final AbsListView view, final int firstVisibleItem,
                         final int visibleItemCount, final int totalItemCount) {
        if (firstVisibleItem + visibleItemCount >= totalItemCount) {
            if (postsLoader.isNotLoadedOrLoading())
                postsLoader.loadNext();
        }
    }


    private View inflateCodeView(final CodeViewHolder holder, final ViewGroup parent) {
        final View v = inflater.inflate(R.layout.code_view_item, parent, false);
        holder.textCode = (TextView) v.findViewById(R.id.code_text_view);
        ViewUtils.applyFont(context, holder.textCode);
        holder.textUsername = (TextView) v.findViewById(R.id.code_author);
        holder.textDescription = (TextView) v.findViewById(R.id.code_descr_text_view);
        holder.comments = (Button) v.findViewById(R.id.commentsCount);
        holder.textVote = (TextView) v.findViewById(R.id.code_vote);
        holder.textDate = (TextView) v.findViewById(R.id.code_date);
        return v;
    }

    public void resetItems() {
        postsLoader.resetPages();
    }

    @Override
    public void onClick(final View v) {
        final int position = (Integer) v.getTag();
        final PostItem item = contentManager.getPostItem(position);
        final Intent intent = new Intent(context, DetailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(DetailActivity.TAG_ITEM, item);

        final AnticipateOvershootInterpolator interpolator = new AnticipateOvershootInterpolator(5.0f, 3.0f);
        v.animate()
                .rotationBy(10.f)
                .translationXBy(20.0f)
                .setDuration(200L)
                .setInterpolator(interpolator)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(final Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(final Animator animation) {
                        v.animate().rotationBy(-10.f).translationXBy(-20.0f).setDuration(100L)
                                .setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                context.startActivity(intent);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                    }

                    @Override
                    public void onAnimationCancel(final Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(final Animator animation) {

                    }
                });

    }

    private void fillCodeView(final CodeViewHolder holder, final int position) {
        final PostItem item = contentManager.getPostItem(position);
        holder.textCode.setText(item.code);
        holder.textDate.setText(item.date);
        holder.textDescription.setText(item.desc);
        holder.textUsername.setText(item.user);
        holder.textVote.setText(item.vote);
        holder.comments.setText('(' + item.commentsCount + ')');
        holder.comments.setTag(position);
        holder.comments.setOnClickListener(this);

        ViewUtils.applyRandomBackgroundColor(holder.textCode);
    }

    private static final class CodeViewHolder {
        TextView textCode;
        TextView textUsername;
        TextView textDescription;
        TextView textVote;
        TextView textDate;
        Button comments;
        int pos = -1;
    }

    private static class NotifyHandler extends Handler {
        private final MainListViewAdapter adapter;

        private NotifyHandler(final MainListViewAdapter adapter) {
            this.adapter = adapter;
        }

        @Override
        public void handleMessage(final Message msg) {
            Logger.log("adapter message recieved " + msg.what);
            if (msg.what == MESSAGE_NOTIFY_DATA_SET_CHANGED) {
                adapter.notifyDataSetChanged();
            }
        }
    }
}
