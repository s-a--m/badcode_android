package com.samart.prj.adapter;

import com.samart.prj.cache.PostsCache;
import com.samart.prj.controller.UiController;
import com.samart.prj.model.ContentManager;
import com.samart.prj.model.ItemsLoaderRunnable;
import com.samart.prj.model.PostItem;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class PostsLoader implements ItemsLoaderRunnable.OnTaskEndedListener {
    private static final ExecutorService EXECUTOR =
            Executors.newSingleThreadExecutor();
    private final MainListViewAdapter adapter;
    private final ContentManager contentManager;
    private final UiController uiController;


    private int lastLoadedPage = -1;
    private int nextPageToLoad;

    public PostsLoader(final MainListViewAdapter adapter, final ContentManager contentManager, UiController uiController) {
        this.adapter = adapter;
        this.contentManager = contentManager;
        this.uiController= uiController;
    }


    /**
     * calls from background thread
     */
    public void notifyAdapter() {
        //   Logger.log("notify adapter");
        adapter.notifyWithHandler();
    }

    /**
     * calls from UI Thread
     *
     * @param totalItemCount
     */
    public void loadNext() {
        //Logger.log("load next " + nextPageToLoad);
        EXECUTOR.execute(
                new ItemsLoaderRunnable(
                        contentManager.g, contentManager.dateItem, contentManager.typeItem, nextPageToLoad, this));
        nextPageToLoad++;
        uiController.setStartLoading();
    }

    public boolean isNotLoadedOrLoading() {
        // Logger.log("is not loaded or loading " + nextPageToLoad + " <= " + lastLoadedPage + ' ' + result);
        return nextPageToLoad == lastLoadedPage + 1;
    }

    /**
     * calls from backgroud thread
     *
     * @param loadedPage
     * @param postItems
     */
    @Override
    public void onSuccess(final int loadedPage, final List<PostItem> postItems) {
        PostsCache.getInstance().addItems(postItems);

        // Logger.log("set loaded nextPageToLoad " + loadedPage);
        lastLoadedPage = loadedPage;
        notifyAdapter();
        uiController.setStopLoading();
    }


    /**
     * calls from background thread
     *
     * @param loadedPage
     */
    @Override
    public void onFail(final int loadedPage) {
        nextPageToLoad--;
        uiController.setStopLoading();
    }

    public void resetPages() {
        lastLoadedPage = -1;
        nextPageToLoad = 0;
        ItemsLoaderRunnable.WorkerContext.lastPage = -1;
    }


}
