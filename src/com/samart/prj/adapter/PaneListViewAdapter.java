package com.samart.prj.adapter;

import android.app.Activity;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.samart.prj.R;
import com.samart.prj.model.ContentManager;
import com.samart.prj.util.Logger;

/**
 * ****************************************************************************
 * Copyright 2013 Dmitry Samoylenko
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ****************************************************************************
 */
public class PaneListViewAdapter extends BaseExpandableListAdapter {
    private final LayoutInflater inflater;
    private static String[] langsNames;
    private static String[] groupNames;
    private static final Object lock = new Object();
    private final View paneView;
    private final ContentManager contentManager;

    public PaneListViewAdapter(final Activity activity, final ContentManager contentManager) {
        if (null == langsNames) {
            synchronized (lock) {
                if (null == langsNames) {
                    final Resources resources = activity.getResources();
                    langsNames = resources.getStringArray(R.array.langs);
                    groupNames = resources.getStringArray(R.array.groups);
                    Logger.log("pane list strings init");
                }
            }
        }
        inflater = activity.getLayoutInflater();
        paneView = createPaneView(activity);
        this.contentManager = contentManager;
    }


    /**
     * calls on left pane items click
     *
     * @param g        item group {@link Groups}
     * @param dateItem required for BEST-category search, means spinner item position, if category==LANG it is a itempos
     * @param typeItem required for BEST-category serarch, means spinner item position
     */
    private void submitQuery(final Groups g, final int dateItem, final int typeItem) {
        contentManager.submitQuery(g, dateItem, typeItem);
    }

    private View createPaneView(final Activity activity) {
        final View v = inflater.inflate(R.layout.pane_viewgroup_best, null);
        final Spinner spinnerDate = (Spinner) v.findViewById(R.id.spinner_date);
        final Resources resources = activity.getResources();
        final String[] dates = resources.getStringArray(R.array.best_date);
        spinnerDate.setAdapter(new ArrayAdapter<String>(activity, R.layout.pane_viewgroup_item, R.id.pane_group_item, dates));
        final Spinner spinnerType = (Spinner) v.findViewById(R.id.spinner_type);
        final String[] types = resources.getStringArray(R.array.best_types);
        spinnerType.setAdapter(new ArrayAdapter<String>(activity, R.layout.pane_viewgroup_item, R.id.pane_group_item, types));
        final Button buttonSubmit = (Button) v.findViewById(R.id.button_show);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final int dateItem = spinnerDate.getSelectedItemPosition();
                final int typeItem = spinnerType.getSelectedItemPosition();
                submitQuery(Groups.BEST, dateItem, typeItem);
            }
        });
        return v;
    }

    public View getPaneView() {
        return paneView;
    }


    public enum Groups implements GroupHandler {
        ALL, LANG {
            @Override
            public int getChildrenCount() {
                return langsNames.length;
            }

            @Override
            public String getText(final int pos) {
                return langsNames[pos];
            }
        }, BEST {
            @Override
            public View getChildView(final int childPosition, final boolean isLastChild, final View convertView, final PaneListViewAdapter adapter, ViewGroup parent) {
                return adapter.getPaneView();
            }
        };

        @Override
        public View getChildView(final int childPosition, final boolean isLastChild, final View convertView, final PaneListViewAdapter adapter, ViewGroup parent) {
            View v = convertView;
            final TextViewHolder holder;
            final Object o = null != v ? v.getTag() : null;
            if (null == v || null == o) {
                v = adapter.inflater.inflate(R.layout.pane_viewgroup_item, parent, false);
                holder = new TextViewHolder();
                holder.textView = (TextView) v.findViewById(R.id.pane_group_item);
                v.setTag(holder);
            } else {
                holder = (TextViewHolder) o;
            }
            if (null == holder)
                Logger.log("null holder! " + name() + ' ' + ordinal() + ' ' + childPosition + ' ' + convertView.toString() + ' ' + o);
            if (holder.pos != childPosition) {
                holder.pos = childPosition;
                holder.textView.setText(getText(childPosition));
                holder.textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        adapter.submitQuery(Groups.this, childPosition, 0);
                    }
                });
            }
            return v;
        }

        @Override
        public String getText(final int pos) {
            return groupNames[pos];
        }


        @Override
        public int getChildrenCount() {
            return 1;
        }

        @Override
        public View getGroupView(final boolean isExpanded, final View convertView, final LayoutInflater inflater, ViewGroup parent) {
            View v = convertView;
            final TextViewHolder holder;
            final Object o = convertView != null ? convertView.getTag() : null;
            if (null == v || null == o) {
                v = inflater.inflate(R.layout.pane_viewgroup_title, parent, false);
                holder = new TextViewHolder();
                holder.textView = (TextView) v.findViewById(R.id.pane_group_title);
                v.setTag(holder);
            } else {
                holder = (TextViewHolder) o;
            }
            final int pos = ordinal();
            if (null == holder) Logger.log("null holder! " + this.name() + ' ' + pos);
            if (holder.pos != pos) {
                holder.pos = pos;
                holder.textView.setText(groupNames[pos]);
            }
            return v;
        }

        public static final Groups[] GROUPSES = Groups.values();

        public static Groups fromPosition(final int pos) {
            return GROUPSES[pos];
        }
    }


    private static class TextViewHolder {
        TextView textView;
        int pos = -1;
    }

    private interface GroupHandler {
        int getChildrenCount();

        View getChildView(int childPosition, boolean isLastChild, View convertView, PaneListViewAdapter adapter, ViewGroup parent);

        View getGroupView(boolean isExpanded, View convertView, LayoutInflater inflater, ViewGroup parent);

        String getText(int pos);
    }

    @Override
    public int getGroupCount() {
        return Groups.GROUPSES.length;
    }

    @Override
    public int getChildrenCount(final int groupPosition) {
        return Groups.fromPosition(groupPosition).getChildrenCount();
    }

    @Override
    public Object getGroup(final int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(final int groupPosition, final int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(final int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(final int groupPosition, final int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, final View convertView, final ViewGroup parent) {
        return Groups.fromPosition(groupPosition).getGroupView(isExpanded, convertView, inflater, parent);
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, final boolean isLastChild, final View convertView, final ViewGroup parent) {
        return Groups.fromPosition(groupPosition).getChildView(childPosition, isLastChild, convertView, this, parent);
    }

    @Override
    public boolean isChildSelectable(final int groupPosition, final int childPosition) {
        return true;
    }
}
